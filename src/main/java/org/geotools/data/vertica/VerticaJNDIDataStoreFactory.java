package org.geotools.data.vertica;

import static org.geotools.data.vertica.VerticaDataStoreFactory.STORAGE_ENGINE;

import java.util.Map;
import org.geotools.jdbc.JDBCJNDIDataStoreFactory;

public class VerticaJNDIDataStoreFactory extends JDBCJNDIDataStoreFactory {
    public VerticaJNDIDataStoreFactory() {
        super(new VerticaDataStoreFactory());
    }

    @Override
    protected void setupParameters(Map parameters) {
        super.setupParameters(parameters);
        parameters.put(STORAGE_ENGINE.key, STORAGE_ENGINE);
    }
}
