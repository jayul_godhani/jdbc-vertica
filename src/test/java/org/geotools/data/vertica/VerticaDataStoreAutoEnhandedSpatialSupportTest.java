/*
 *    GeoTools - The Open Source Java GIS Toolkit
 *    http://geotools.org
 *
 *    (C) 2017, Open Source Geospatial Foundation (OSGeo)
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation;
 *    version 2.1 of the License.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 */
package org.geotools.data.vertica;

import org.geotools.jdbc.JDBCTestSetup;
import org.geotools.jdbc.JDBCTestSupport;

/**
 * Tests that enhandedSpatialSupport flag is automatically and properly set based on identified
 * MySQL version.
 *
 * @author Justin Deoliveira, The Open Planning Project
 */
public class VerticaDataStoreAutoEnhandedSpatialSupportTest extends JDBCTestSupport {
    @Override
    protected JDBCTestSetup createTestSetup() {
        return new VerticaTestSetup();
    }

    public void testEnhancedSpatialSupportDetection() throws Exception {
        boolean isMySQL56 = VerticaDataStoreFactory.isMySqlVersion56OrAbove(dataStore);
        boolean isMySQL80 = VerticaDataStoreFactory.isMySqlVersion80OrAbove(dataStore);
        if (isMySQL56) {
            assertTrue(((VerticaDialectBasic) dialect).getUsePreciseSpatialOps());
        } else if (isMySQL80) {
            assertTrue(((VerticaDialectBasic) dialect).getUsePreciseSpatialOps());
        } else {
            assertFalse(((VerticaDialectBasic) dialect).getUsePreciseSpatialOps());
        }
    }
}
