package org.geotools.data.vertica.jndi;

import org.geotools.data.vertica.VerticaTestSetup;
import org.geotools.jdbc.JDBCDataStoreOnlineTest;
import org.geotools.jdbc.JDBCJNDITestSetup;
import org.geotools.jdbc.JDBCTestSetup;

public class VerticaDataStoreOnlineTest extends JDBCDataStoreOnlineTest {

    @Override
    protected JDBCTestSetup createTestSetup() {
        return new JDBCJNDITestSetup(new VerticaTestSetup());
    }

    @Override
    protected String getCLOBTypeName() {
        // CLOB is supported in MySQL 8 but not in 5
        return "TEXT";
    }
}
